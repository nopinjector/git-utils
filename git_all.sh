#!/bin/bash

while read p; do
	echo "==================================================================================="
	echo ">>>>>>>>>>>> $p "
	echo "==================================================================================="
	git -C $p "$@"
done < ./.gitdirs
