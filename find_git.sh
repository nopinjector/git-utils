#!/bin/sh

BASEPATH="$(realpath -e "$1")"

find "${BASEPATH}" -maxdepth 5 -type d -name '.git' -printf "${BASEPATH}/%P\n" | sed -e 's/.git$//'
