#!/bin/bash

while read p; do
	BRANCH_NAME=`git -C "$p" rev-parse --abbrev-ref HEAD 2>/dev/null`
	COMMITS_BEHIND=`git -C "$p" log @..@{u} --oneline 2>/dev/null | wc -l`
	COMMITS_AHEAD=`git -C "$p" log @{u}..@ --oneline 2>/dev/null | wc -l`
	#REP_INFO=`git -C "$p" log -1 --pretty=format:"%h%x09%ad%x09%an%x09%s"`

	if [ $COMMITS_AHEAD -ne 0 ]; then
		COMMITS_AHEAD=`printf "+%-5d" $COMMITS_AHEAD`
	else
		COMMITS_AHEAD="      "
	fi

	if [ $COMMITS_BEHIND -ne 0 ]; then
		COMMITS_BEHIND=`printf "...%-5d" $COMMITS_BEHIND`
	else
		COMMITS_BEHIND="        "
	fi
	printf "%-70s %-20s %s %s %s\n" "$p" "$BRANCH_NAME" "$COMMITS_BEHIND" "$COMMITS_AHEAD" "$REP_INFO"
	#printf "%-70s %-20s ...%-5d +%-5d %s\n" "$p" "$BRANCH_NAME" "$COMMITS_BEHIND" "$COMMITS_AHEAD" "$REP_INFO"

done < ~/.gitdirs
