#!/bin/bash

while read p; do
	git -C $p merge --ff-only
done < ~/.gitdirs
